FROM nginx:stable

ARG SERVER_NAME=test-dokuwiki.priv
LABEL maintainer="Jonas Chopin-Revel"
LABEL name="nginx-dokuwiki"
LABEL version="2.0.1"


RUN apt-get update && \
DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" curl openssl \
&& apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 -subj '/CN=selfsigned' -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt

RUN rm /etc/nginx/conf.d/default.conf

COPY --chown=www-data:www-data conf-nginx/vhost.conf /etc/nginx/conf.d/

WORKDIR /var/www/dokuwiki

RUN curl -LS https://download.dokuwiki.org/src/dokuwiki/dokuwiki-stable.tgz -o /tmp/dokuwiki-stable.tgz && \
tar -xzvf /tmp/dokuwiki-stable.tgz --strip-components=1 && \
rm -f /tmp/dokuwiki-stable.tgz && \
chown www-data: -R /var/www/dokuwiki && \
chmod 755 -R /var/www/dokuwiki

VOLUME /var/www/dokuwiki
VOLUME /etc/nginx

EXPOSE 80 443
