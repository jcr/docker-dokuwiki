# Dokuwiki with Docker - Version 2.0.1

## Usage
```yaml
# Build nginx-dokuwiki image
docker build [--build-arg SERVER_NAME=example.org] -t nginx-dokuwiki .

# Deploy containers
docker-compose -f docker-compose.yaml up -d

# Add Dokuwiki sources in the server root
docker cp -a dokuwiki/ nginx:/var/www/
```

## Nota Bene

### Requirements
* Docker
* Docker-compose

### Dokuwiki sources
In Docker Nginx image, last stable Dokuwiki release (2020-07-29 "Hogfather") sources are downloaded in /var/www/dokuwiki. You're free to change these files in order to put sources of your own wiki or another version.
Your sources must have www-data as owner and group.

### SSL certificate
Dockerfile creates a selfsigned certificate and the vhost nginx configuration file uses this certificate.

If you want to change it :
1. Comment this line in the Dockerfile :
```RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 -subj '/CN=selfsigned' -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt```
2. Change ssl_certificate and ssl_certificate_key path in vhost.conf


### Nginx configuration
#### server_name
By default, server_name in vhost.conf is test-dokuwiki.priv. Don't forget to adapt it with your own domain name in the docker build command :
```docker build --build-arg SERVER_NAME=example.org -t nginx-dokuwiki .```

#### Vhost.conf configuration
In vhost.conf, ```location ~ /(conf/|bin/|inc/|install.php) { deny all; }``` (line 28) is commented in order to permit installation of Dokuwiki. If you have finished installation or want directly to upload you own wiki, don't forget to uncomment this line.
## Credits

Tool developped by Jonas Chopin-Revel on Licence GNU-GPLv3.

Gitlab : https://framagit.org/jcr/docker-dokuwiki

**Share and improve it. It's free !**
